#!/usr/bin/env python3
"""
Script for continuous temperature and humidity measurement
on a DHT11 sensor module 
"""

import datetime
import logging
import os
import seeed_dht
import time

# in second
DELAY = 5*60

# TODO remove the DEBUG level when in production
logging.getLogger().setLevel(logging.DEBUG)

# current path
path = os.path.realpath(__file__)
path = path[:path.rfind(os.sep)]
logging.debug('path=' + path)

# output json file path
filename = __file__.replace(path, '')

# case relative path execution ./MY_FILENAME.py
if filename.rfind(os.sep):
    filename = filename[filename.rfind(os.sep)+1:]

filename = filename[0:filename.rfind('.py')]
filepath = os.environ['HOME'] + os.sep + ".config" + os.sep + filename + '.csv'
logging.debug('filepath=' + filepath)

# pin=4 means GPIO4 == PIN7
sensor = seeed_dht.DHT("11", pin=4)

# header
if not os.path.exists(filepath):
    f = open(filepath, 'w')
    f.write('humidity (%),temperature (°C),hardware model' + os.linesep)
    f.close()

# infinite loop: 1 mesure per DELAY
f = open(filepath, 'a+')
while True:
    humi, temp = sensor.read()
    d = datetime.datetime.now().isoformat()
    f.write('{0},{1:.1f},{2:.1f},DHT{3}'.format(
        d, humi, temp, sensor.dht_type) + os.linesep)
    f.flush()  # mandatory to avoid data loss when the battery will by empty
    time.sleep(DELAY)
