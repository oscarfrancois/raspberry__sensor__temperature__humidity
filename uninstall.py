#!/usr/bin/env python3

import os

os.system("systemctl --user stop sensor__humidity__temperature.service")
os.system("systemctl --user disable sensor__humidity__temperature.service")
os.system("systemctl --user daemon-reload")
os.system("rm ~/.config/systemd/user/sensor__humidity__temperature.service") 
os.system("rm ~/.local/bin/sensor__humidity__temperature.py")
