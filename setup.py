#!/usr/bin/env python3

import os

os.system("mkdir -p ~/.local/bin")
os.system("cp sensor__humidity__temperature.py ~/.local/bin")
os.system("chmod a+x ~/.local/bin/sensor__humidity__temperature.py")

os.system("mkdir -p ~/.config/systemd/user")
os.system("cp sensor__humidity__temperature.service ~/.config/systemd/user") 
os.system("systemctl --user enable sensor__humidity__temperature.service")
os.system("systemctl --user daemon-reload")
os.system("systemctl --user start sensor__humidity__temperature.service")
os.system("systemctl --user status sensor__humidity__temperature.service")
