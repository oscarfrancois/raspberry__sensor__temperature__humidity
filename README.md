# Setup of a `DHT11` temperature and humidity sensor on a raspberry pi zero.

# Disclaimer of liability

Electrical devices can be hazardous. Use with caution. Only a qualified person is allowed to apply this tutorial.

cf. LICENSE for more information.

# Raspberry pi zero pinout
![raspberry pi zero pinout](res/img/pi_zero__gpio.jpg)

# Grove DHT11 product overview
![Grove DHT11 product overview](res/img/sensor__dht11.jpg)

# Wiring

![final setup example](res/img/wiring.png)

RPI|Humidity sensor
---|---
GPIO12|SIG
not connected|NC
3V3|VCC
GPIO25|GND

# Software install
```
sudo apt install python3-pip
pip3 install -r requirements.txt
sudo loginctl enable-linger $LOGNAME
./setup.py
```

## Remarks

A cache folder contains the source code of the dependencies.
